manufacturer = Facter.value(:dmi)['manufacturer']

Facter.add(:hp) do
  confine :virtual => 'physical'
  setcode do
    if ( manufacturer =~ /^HP(E)?$/ )
      hp = {}
      hp['generation'] = Facter.value(:dmi)['product']['name'].match(/G(en)?(?<generation>\d+)/)[:generation].to_i
      hp
    end
  end
end