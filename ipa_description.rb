#!/usr/bin/env ruby
#
# ipa_description.rb
# Create fact based on description from FreeIPA (or IdM).
#
#
# Depends on Puppetforge module: waveclaw/facter_cacheable
# Depends on working ldap environment (managed /etc/openldap/ldap.conf)
# Depends on Kerberos host keytab
# Caches facts in /etc/facter/facts.d/ipa_description.yaml for 24h.
#
require 'facter'
require 'socket'

begin
  require 'facter/util/facter_cacheable'
rescue LoadError => e
  Facter.debug("#{e.backtrace[0]}: #{$ERROR_INFO}.")
end

keytab='/etc/krb5.keytab'
hostname=Socket.gethostname

Facter.add(:ipa_description) do
  confine { Puppet.features.facter_cacheable? }
  confine { File.exist? '/bin/ldapsearch' or File.exist? '/usr/bin/ldapsearch' }
  confine { File.exist? '/etc/krb5.keytab' }
  setcode do
    cache = Facter::Util::FacterCacheable.cached?(:ipa_description, 24 * 3600)
    if ! cache
      description = ""
      kinit_cmd = 'kinit -k -t %<keytab>s host/%<fqdn>s >/dev/null 2>&1' % {keytab: keytab, fqdn: hostname}
      retval = system(kinit_cmd)

      if retval
        ldapsearch_cmd = "ldapsearch -QLLL -Y GSSAPI fqdn=%<fqdn>s description | awk -F':' '/^description/ { print $2 }'" % {fqdn: hostname}
        description = %x[ #{ldapsearch_cmd} ].strip

        if ! description.empty?
          # store the value for later
          Facter::Util::FacterCacheable.cache(:ipa_description, description)
          # return the expensive value
          description
        else
          "unknown"
        end
      else
        "unknown."
      end
    else
      # return the cached value (this may need processing)
      cache
    end
  end
end